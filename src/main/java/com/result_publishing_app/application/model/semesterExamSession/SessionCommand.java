package com.result_publishing_app.application.model.semesterExamSession;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SessionCommand extends SessionResponse{
}
