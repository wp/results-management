package com.result_publishing_app.application.model.semesterExamSession;

import com.result_publishing_app.application.model.enums.ExamSession;
import com.result_publishing_app.application.model.semester.Semester;
import com.result_publishing_app.application.model.enums.StudyCycle;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SemesterExamSession {

    // 2022/2023-W-JUNE
    @Id
    private String name;

    @Enumerated(EnumType.STRING)
    private ExamSession session;

    // 2022/2023
    private String year;

    private LocalDate start;

    private LocalDate end;

    private LocalDate enrollmentStartDate;

    private LocalDate enrollmentEndDate;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    private List<StudyCycle> cycle;

}

