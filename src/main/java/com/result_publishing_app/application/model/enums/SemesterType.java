package com.result_publishing_app.application.model.enums;

public enum SemesterType {
    WINTER,
    SUMMER
}
