package com.result_publishing_app.application.model.enums;

public enum RoomType {
    CLASSROOM, LAB, MEETING_ROOM, OFFICE
}
