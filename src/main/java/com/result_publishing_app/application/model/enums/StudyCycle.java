package com.result_publishing_app.application.model.enums;

public enum StudyCycle {

    UNDERGRADUATE, MASTER, PHD
}

