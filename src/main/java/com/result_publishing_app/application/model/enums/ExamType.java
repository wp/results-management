package com.result_publishing_app.application.model.enums;

public enum ExamType {
    LAB, CLASSROOM, ONLINE, HOMEWORK
}
