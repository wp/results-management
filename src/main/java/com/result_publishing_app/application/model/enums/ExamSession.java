package com.result_publishing_app.application.model.enums;

public enum ExamSession {
    FIRST_MIDTERM, SECOND_MIDTERM, JANUARY, JUNE, SEPTEMBER
}
