package com.result_publishing_app.application.exceptions;

public class ResultNotFoundException extends RuntimeException{
    public ResultNotFoundException(Long id) {
        super(String.format("Result with id %d was not found", id));
    }
}
