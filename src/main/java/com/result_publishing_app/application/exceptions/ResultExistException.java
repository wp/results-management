package com.result_publishing_app.application.exceptions;

import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;

public class ResultExistException extends RuntimeException{
    public ResultExistException(SemesterExamSession session, CourseGroup courseGroup) {
        super(String.format("Result for session %s and courseGroup %s-%s already exists",
                session.getName(),
                courseGroup.getCourse().getId(),
                courseGroup.getProfessor().getId()));
    }
}
