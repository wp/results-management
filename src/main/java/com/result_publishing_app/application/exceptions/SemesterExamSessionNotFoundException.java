package com.result_publishing_app.application.exceptions;

public class SemesterExamSessionNotFoundException extends RuntimeException{
    public SemesterExamSessionNotFoundException(String name) {
        super(String.format("SemesterExamSession with name %s was not found", name));
    }
}
