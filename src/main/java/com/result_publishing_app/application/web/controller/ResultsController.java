package com.result_publishing_app.application.web.controller;


import com.result_publishing_app.application.model.results.Results;
import com.result_publishing_app.application.service.ResultsService;
import com.result_publishing_app.application.service.SemesterExamSessionService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;
import com.result_publishing_app.application.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


@Controller
@RequestMapping("/admin/results")
public class ResultsController {

    private final ResultsService resultsService;
    private final CourseService courseService;
    private final SemesterExamSessionService semesterExamSessionService;
    private final CourseGroupService courseGroupService;
    private final ProfessorService professorService;

    public ResultsController(ResultsService resultsService, CourseService courseService, SemesterExamSessionService semesterExamSessionService, CourseGroupService courseGroupService, ProfessorService professorService) {
        this.resultsService = resultsService;
        this.courseService = courseService;
        this.semesterExamSessionService = semesterExamSessionService;
        this.courseGroupService = courseGroupService;
        this.professorService = professorService;
    }


    @GetMapping("/all")
    public String getAllResultsPage(Model model,
                                    @RequestParam(defaultValue = "") String course,
                                    @RequestParam(defaultValue = "") String professor,
                                    @RequestParam(defaultValue = "") String semesterExamSession,
                                    @RequestParam(defaultValue = "false") Boolean hasResults,
                                    @RequestParam(defaultValue = "1") Integer pageNum,
                                    @RequestParam(defaultValue = "10") Integer results) {
        model.addAttribute("courses", courseService.findAll());
        model.addAttribute("semesterExamSessions", semesterExamSessionService.findAll());
        model.addAttribute("professors", professorService.findAllDistinct());
        List<CourseGroup> courseGroups = courseGroupService.findByCourseAndProfessor(course, professor);
        model.addAttribute("page", resultsService.filterAndPaginateResults(courseGroups, semesterExamSession,
                hasResults, pageNum, results));
        return "resultsAll";
    }

    @GetMapping("/{id}/upload")
    public String uploadPage(@PathVariable Long id, Model model) {
        model.addAttribute("result", this.resultsService.findById(id));
        return "importResults";
    }

    @GetMapping("/{professor}")
    public String getResultsByProfessor(@PathVariable String professor, Model model,
                                        @RequestParam(defaultValue = "") String course,
                                        @RequestParam(defaultValue = "") String semesterExamSession,
                                        @RequestParam(defaultValue = "false") Boolean hasResults,
                                        @RequestParam(defaultValue = "1") Integer pageNum,
                                        @RequestParam(defaultValue = "10") Integer results) {

        model.addAttribute("courses", courseService.findAll());
        model.addAttribute("semesterExamSessions", semesterExamSessionService.findAll());
        model.addAttribute("prof", this.professorService.findById(professor));
        List<CourseGroup> courseGroups = courseGroupService.findByCourseAndProfessor(course, professor);
        model.addAttribute("page", resultsService.filterAndPaginateResults(courseGroups, semesterExamSession,
                hasResults, pageNum, results));
        return "resultsByProf";
    }


    @PostMapping("/{id}/upload")
    public String uploadPdf(@PathVariable Long id,
                            @RequestParam String professor,
                            @RequestParam @NotNull MultipartFile pdfFile,
                            RedirectAttributes redirectAttributes) {

        try {
            this.resultsService.savePdf(id, pdfFile);
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

        redirectAttributes.addAttribute("professor", professor);

        return "redirect:/admin/results/{professor}";

    }

    @GetMapping("/DownloadResultPdf/{resultId}")
    public HttpEntity<byte[]> DownloadPdfResult(@PathVariable Long resultId) {
        Results results = resultsService.findById(resultId).orElseThrow();
        byte[] pdfResultByte = results.getPdfBytes();
        String filename = "Results.pdf";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_PDF);
        httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        httpHeaders.setContentLength(pdfResultByte.length);
        return new HttpEntity<>(pdfResultByte, httpHeaders);
    }


    @GetMapping("/initializeExamSession")
    public String initializeExamSessionPage(Model model,
                                            @RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "50") int size,
                                            @RequestParam(defaultValue = "start") String sortBy) {
        model.addAttribute("examSessions",
                this.semesterExamSessionService.findAllWithPaginationAndSorting(page, size, sortBy).getContent());
        return "initializeExamSession";
    }


    @PostMapping("/initializeExamSession")
    public String createEmptyResults(@RequestParam String name) {
        this.semesterExamSessionService.initializeExamSession(name);

        return "resultsAll";
    }
}
