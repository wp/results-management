package com.result_publishing_app.application.repository;

import com.result_publishing_app.application.model.enums.ExamSession;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface SemesterExamSessionRepository extends JpaRepository<SemesterExamSession,String> {

    Optional<SemesterExamSession> findByName(String name);

    /*Optional<Session> findByName(String string);

    boolean existsByName(String name);

    boolean existsByDueDateAndName(LocalDate dueDate,String name);

    List<Session> findByCourses_Id(String id);*/
}
