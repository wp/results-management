package com.result_publishing_app.application.repository;

import com.result_publishing_app.application.model.studentCourses.StudentCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentInCourses extends JpaRepository<StudentCourses, Long> {
}
