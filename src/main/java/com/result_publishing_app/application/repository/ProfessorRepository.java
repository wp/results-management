package com.result_publishing_app.application.repository;

import com.result_publishing_app.application.model.professor.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor,String> {

    @Query(value = "SELECT distinct(p.id), p.name, p.email, p.title " +
            "FROM professor as p " +
            "JOIN course_group as cg on cg.professor_id = p.id", nativeQuery = true)
    List<Professor> findDistinctById();

    /*List<Professor> findByName(String name);

    Optional<Professor> findByEmail(String email);
    Boolean existsByEmail(String email);

    List<Professor> findByCourses_Id(String id);*/
}
