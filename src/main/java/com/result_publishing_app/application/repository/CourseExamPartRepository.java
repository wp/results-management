package com.result_publishing_app.application.repository;

import com.result_publishing_app.application.model.courseExamPart.CourseExamPart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseExamPartRepository extends JpaRepository<CourseExamPart, String> {
}
