package com.result_publishing_app.application.repository;



import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.results.Results;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ResultsRepository extends JpaRepository<Results,Long> {

    Optional<Results> findBySessionAndCourseGroup(SemesterExamSession session, CourseGroup courseGroup);

    Page<Results> findByCourseGroupInAndSessionNameAndPdfBytesIsNull(List<CourseGroup> courseGroups, String name, Pageable pageable);

    Page<Results> findByCourseGroupInAndPdfBytesIsNull(List<CourseGroup> courseGroups, Pageable pageable);

    Page<Results> findBySessionNameAndPdfBytesIsNull(String Name, Pageable pageable);

    Page<Results> findByPdfBytesIsNull(Pageable pageable);

    Page<Results> findByCourseGroupInAndSessionNameAndPdfBytesIsNotNull(List<CourseGroup> courseGroups, String name, Pageable pageable);

    Page<Results> findByCourseGroupInAndPdfBytesIsNotNull(List<CourseGroup> courseGroups, Pageable pageable);

    Page<Results> findBySessionNameAndPdfBytesIsNotNull(String Name, Pageable pageable);

    Page<Results> findByPdfBytesIsNotNull(Pageable pageable);


    /*@Query(value = "SELECT r.id, r.course_group_id, r.session_name, r.pdf_bytes, r.uploaded_at " +
            "FROM results as r " +
            "JOIN ")
    Page<Results> findByCourseGroupId(@Param())*/
    /*List<Results> findBySession(Session session);

    List<Results> findByCourse(Course course);

    @Query(value = "SELECT r.id,r.course_id,r.session_name,r.pdf FROM results as r " +
            "JOIN professor_course as pc on pc.course_id = r.course_id " +
            "JOIN professor as p on p.id = pc.professor_id " +
            "WHERE p.id=:professorId",nativeQuery = true)
    List<Results> findByProfessor(@Param("professorId") String professorId);

    @Query(value = "SELECT r.id,r.course_id,r.session_name,r.pdf FROM results as r " +
            "JOIN student_course as sc on r.course_id = sc.course_id " +
            "JOIN student as s on s.index = sc.student_index " +
            "WHERE s.index=:index",nativeQuery = true)
    List<Results> findByStudent(@Param("index") String index);*/
}
