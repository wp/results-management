package com.result_publishing_app.application.repository;

import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseGroupRepository extends JpaRepository<CourseGroup, Long> {

    List<CourseGroup> findByCourseIdAndProfessorId(String courseId,  String professorId);

    List<CourseGroup> findByCourseId(String courseId);

    List<CourseGroup> findByProfessorId(String professorId);

    List<CourseGroup> findAllByCourse_Semester_Year(String year);
}
