package com.result_publishing_app.application.service;

import com.result_publishing_app.application.exceptions.SemesterExamSessionNotFoundException;
import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.enums.ExamSession;
import com.result_publishing_app.application.model.results.Results;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;
import com.result_publishing_app.application.repository.CourseGroupRepository;
import com.result_publishing_app.application.repository.ResultsRepository;
import com.result_publishing_app.application.repository.SemesterExamSessionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;
import com.result_publishing_app.application.repository.SemesterExamSessionRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SemesterExamSessionService {

    private final SemesterExamSessionRepository semesterExamSessionRepository;
    private final CourseGroupRepository courseGroupRepository;
    private final ResultsRepository resultsRepository;

    public SemesterExamSessionService(SemesterExamSessionRepository semesterExamSessionRepository, CourseGroupRepository courseGroupRepository, ResultsRepository resultsRepository) {
        this.semesterExamSessionRepository = semesterExamSessionRepository;
        this.courseGroupRepository = courseGroupRepository;
        this.resultsRepository = resultsRepository;
    }

    public Optional<SemesterExamSession> findByName(String name) {
        return this.semesterExamSessionRepository.findByName(name);
    }

    public List<SemesterExamSession> findAll() {
        return semesterExamSessionRepository.findAll();
    }

    public Page<SemesterExamSession> findAllWithPaginationAndSorting(Integer page, Integer size, String sortBy) {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.DESC, sortBy, Sort.NullHandling.NULLS_LAST));
        Pageable pageable = PageRequest.of(page, size, sort);

        return semesterExamSessionRepository.findAll(pageable);
    }

    public void createResultForExamSessionAndCourseGroup(SemesterExamSession session, CourseGroup courseGroup) {

        boolean exists = this.resultsRepository.findBySessionAndCourseGroup(session, courseGroup)
                .isPresent();

        if (!exists) {
            Results results = new Results();
            results.setSession(session);
            results.setCourseGroup(courseGroup);

            this.resultsRepository.save(results);
        }
    }

    public void initializeExamSession(String name) {
        SemesterExamSession semesterExamSession = this.semesterExamSessionRepository.findByName(name)
                .orElseThrow(() -> new SemesterExamSessionNotFoundException(name));

        List<CourseGroup> courseGroupList = this.courseGroupRepository
                .findAllByCourse_Semester_Year(semesterExamSession.getYear());


        courseGroupList
                .forEach(courseGroup -> this.createResultForExamSessionAndCourseGroup(semesterExamSession, courseGroup));
    }



    /*@Autowired
    SessionRepository sessionRepository;

    private SemesterExamSessionRepository semesterExamSessionRepository;


    public SemesterExamSessionService(SemesterExamSessionRepository semesterExamSessionRepository) {
        this.semesterExamSessionRepository = semesterExamSessionRepository;
    }

    public List<SemesterExamSession> findAll(){
        return semesterExamSessionRepository.findAll();
    }

    /*@Autowired
    SessionMapper sessionMapper;

    public Session findByName(String name){
        return sessionRepository.findByName(name).orElseThrow(()->new SessionNotFoundException(String
                .format("Session with id %s does not exist",name)));
    }

    public Session create(SessionCommand command){

        if (sessionRepository.existsByName(command.getName()))
            throw new SessionExistException(String.
                    format("Session with name %s exists",command.getDueDate().toString(),
                            command.getName()));
        else
            return sessionRepository.save(sessionMapper.commandToModel(command));
    }

    public Session update(SessionCommand command){

        Session session= sessionRepository.findByName(command.getName()).orElseThrow(()->new SessionNotFoundException(String
                .format("Session with name %s does not exist",command.getName())));
        sessionMapper.updateSession(command,session);

        return sessionRepository.save(session);

    }

    public void  delete(String name){
        Session session= sessionRepository.findByName(name).orElseThrow(()->new SessionNotFoundException(String
                .format("Session with name %s does not exist",name)));
        sessionRepository.delete(session);
    }*/
}
