package com.result_publishing_app.application.service;

import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.repository.CourseGroupRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseGroupService {

    private CourseGroupRepository courseGroupRepository;

    public CourseGroupService(CourseGroupRepository courseGroupRepository) {
        this.courseGroupRepository = courseGroupRepository;
    }

    public List<CourseGroup> findAll(){
        return courseGroupRepository.findAll();
    }

    public List<CourseGroup> findByCourseAndProfessor(String courseId,  String professorId){
        if(!courseId.isEmpty() && !professorId.isEmpty()){
            return courseGroupRepository.findByCourseIdAndProfessorId(courseId, professorId);
        }
        else if(!courseId.isEmpty()){
            return courseGroupRepository.findByCourseId(courseId);
        }
        else if(!professorId.isEmpty()){
            return courseGroupRepository.findByProfessorId(professorId);
        }
        return null;
    }
}
