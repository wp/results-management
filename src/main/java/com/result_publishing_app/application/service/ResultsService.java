package com.result_publishing_app.application.service;

import com.result_publishing_app.application.exceptions.*;
import com.result_publishing_app.application.mapper.ResultsMapper;
import com.result_publishing_app.application.model.course.Course;
import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.enums.ExamSession;
import com.result_publishing_app.application.model.professor.Professor;
import com.result_publishing_app.application.model.courseGroup.CourseGroup;
import com.result_publishing_app.application.model.results.Results;

import com.result_publishing_app.application.model.results.ResultsResponse;
import com.result_publishing_app.application.model.semesterExamSession.SemesterExamSession;
import com.result_publishing_app.application.model.student.Student;

import com.result_publishing_app.application.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@Service
public class ResultsService {

    private final ResultsRepository resultsRepository;
    private final CourseGroupRepository courseGroupRepository;
    private final SemesterExamSessionRepository semesterExamSessionRepository;

    public ResultsService(ResultsRepository resultsRepository, CourseGroupRepository courseGroupRepository, SemesterExamSessionRepository semesterExamSessionRepository) {
        this.resultsRepository = resultsRepository;
        this.courseGroupRepository = courseGroupRepository;
        this.semesterExamSessionRepository = semesterExamSessionRepository;
    }

    public Optional<Results> findById(Long id) {
        return this.resultsRepository.findById(id);
    }

    public Optional<Results> savePdf(Long id, MultipartFile pdfFile) throws IOException {
        Results result = this.findById(id)
                .orElseThrow(() -> new ResultNotFoundException(id));

        byte[] pdfBytes = pdfFile.getBytes();

        result.setPdfBytes(pdfBytes);
        result.setUploadedAt(LocalDateTime.now());

        return Optional.of(this.resultsRepository.save(result));
    }


    public Page<Results> findAllWithPagination(Integer page, Integer size) {
        return resultsRepository.findAll(PageRequest.of(page, size));
    }

    public Page<Results> filterAndPaginateResults(List<CourseGroup> courseGroups, String semesterExamSessionName, Boolean hasResults,
                                                  Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        if (hasResults) {
            if (courseGroups != null && !semesterExamSessionName.isEmpty()) {
                return resultsRepository.findByCourseGroupInAndSessionNameAndPdfBytesIsNotNull(courseGroups,
                        semesterExamSessionName, pageRequest);
            } else if (courseGroups != null) {
                return resultsRepository.findByCourseGroupInAndPdfBytesIsNotNull(courseGroups, pageRequest);
            } else if (!semesterExamSessionName.isEmpty()) {
                return resultsRepository.findBySessionNameAndPdfBytesIsNotNull(semesterExamSessionName, pageRequest);
            }
            return resultsRepository.findByPdfBytesIsNotNull(pageRequest);
        } else {
            if (courseGroups != null && !semesterExamSessionName.isEmpty()) {
                return resultsRepository.findByCourseGroupInAndSessionNameAndPdfBytesIsNull(courseGroups,
                        semesterExamSessionName, pageRequest);
            } else if (courseGroups != null) {
                return resultsRepository.findByCourseGroupInAndPdfBytesIsNull(courseGroups, pageRequest);
            } else if (!semesterExamSessionName.isEmpty()) {
                return resultsRepository.findBySessionNameAndPdfBytesIsNull(semesterExamSessionName, pageRequest);
            }
            return resultsRepository.findByPdfBytesIsNull(pageRequest);
        }
    }




    public void createResultForExamSessionAndCourseGroup(SemesterExamSession session, CourseGroup courseGroup) {

        boolean exists = this.resultsRepository.findBySessionAndCourseGroup(session, courseGroup)
                .isPresent();

        if (!exists) {
            Results results = new Results();
            results.setSession(session);
            results.setCourseGroup(courseGroup);

            this.resultsRepository.save(results);
        }
    }

    public void initializeForExamSession(String name) {
        SemesterExamSession semesterExamSession = this.semesterExamSessionRepository.findByName(name)
                .orElseThrow(() -> new SemesterExamSessionNotFoundException(name));

        List<CourseGroup> courseGroupList = this.courseGroupRepository
                .findAllByCourse_Semester_Year(semesterExamSession.getYear());


        courseGroupList
                .forEach(courseGroup -> this.createResultForExamSessionAndCourseGroup(semesterExamSession, courseGroup));
    }






    /*@Autowired
    SemesterExamSessionRepository sessionRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    ProfessorRepository professorRepository;

    @Autowired
    ResultsMapper resultsMapper;

    public List<ResultsResponse> findBySession(String sessionName){

        Session session=sessionRepository.findByName(sessionName).orElseThrow(()->new SessionNotFoundException(String
                .format("Session with id %s does not exist",sessionName)));

        return resultsMapper.modelToResponse(resultsRepository.findBySession(session));
    }

    public List<ResultsResponse> findByCourse(String courseId){
        Course course=courseRepository.findById(courseId).orElseThrow(()->new CourseNotFoundException(String
                .format("Course with id %s does not exist",courseId)));

        return resultsMapper.modelToResponse(resultsRepository.findByCourse(course));
    }

    public List<ResultsResponse> findByStudent(String index){
        Student student= studentRepository.findById(index).orElseThrow(()->new StudentNotFoundException(String.
                format("Student with id %s does not exist",index)));

        return resultsMapper.modelToResponse(resultsRepository.findByStudent(index));
    }

    public List<ResultsResponse> findByProfessor(String professorId){
        Professor professor= professorRepository.findById(professorId).orElseThrow(()-> new ProfessorNotFoundException(professorId));

        return resultsMapper.modelToResponse(resultsRepository.findByProfessor(professorId));
    }

    public void createResultWithPdf(MultipartFile pdfFile, String courseId, String sessionName) throws IOException {

        Session session=sessionRepository.findByName(sessionName).orElseThrow(()->new SessionNotFoundException(String
                .format("Session with id %s does not exist",sessionName)));

        Course course=courseRepository.findById(courseId).orElseThrow(()->new CourseNotFoundException(String
                .format("Course with id %s does not exist",courseId)));

        byte[] pdfBytes = pdfFile.getBytes();

        Results results=new Results();

        results.setCourse(course);
        results.setSession(session);
        results.setPdfBytes(pdfBytes);

        resultsRepository.save(results);
    }*/
}
