package com.result_publishing_app.application.service;

import com.result_publishing_app.application.model.professor.Professor;
import com.result_publishing_app.application.repository.ProfessorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessorService {

    private ProfessorRepository professorRepository;

    public ProfessorService(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    public List<Professor> findAll() {
        return professorRepository.findAll();
    }

    public List<Professor> findAllDistinct(){
        return professorRepository.findDistinctById();
    }

    public Professor findById(String professor) {
        return professorRepository.findById(professor).orElse(null);
    }

    /*@Autowired
    ProfessorMapper mapper;

    public Professor findById(String id) {
        return professorRepository.findById(id).orElseThrow(() -> new ProfessorNotFoundException(id));
    }

    public Professor findByEmail(String email) {
        return professorRepository.findByEmail(email).orElse(null);
    }

    public Professor createProfessor(ProfessorCommand command) {
        if (professorRepository.existsById(command.getId()))
            throw new ProfessorExistException(String.
                    format("Professor with id %s already exist", command.getId()));

        else if (professorRepository.existsByEmail(command.getEmail()))
            throw new ProfessorExistException(String.
                    format("Professor with email %s already exist", command.getEmail()));
        else {
            Professor professor = mapper.commandToModel(command);
            return professorRepository.save(professor);
        }
    }

    public Professor updateProfessor(ProfessorCommand command) {

        Professor professor = professorRepository.findById(command.getId()).orElseThrow(() -> new ProfessorNotFoundException(command.getId()));

        mapper.updateProfessor(command, professor);

        professorRepository.save(professor);

        return professor;

    }

    public void deleteProfessor(String id) {
        Professor professor = professorRepository.findById(id).orElseThrow(() -> new ProfessorNotFoundException(id));

        professorRepository.delete(professor);
    }

    @Transactional
    public void importCsv(MultipartFile file) throws IOException, CsvValidationException {
        CSVReader csvReader = new CSVReader(new InputStreamReader(file.getInputStream()));
        String[] line;
        csvReader.readNext();
        while ((line = csvReader.readNext()) != null) {
            if (professorRepository.existsById(line[0]))
                throw new ProfessorExistException(String.format("Professor with id %s already exist",line[0]));
            Professor professor = new Professor();
            professor.setId(line[0]);
            professor.setName(line[1]);
            professor.setEmail(line[2]);
            professor.setRole(ProfessorRole.valueOf(line[3]));
            professorRepository.save(professor);
        }
    }*/
}
