INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Веб програмирање', null, null, '2022-23-W', 'F23L3W024', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Вовед во науката за податоци', null, null, '2022-23-W', 'F23L3W008', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Бази на податоци', null, null, '2022-23-W', 'F23L3W004', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Електронска и мобилна трговија', null, null, '2022-23-S', 'F23L3S025', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Дизајн на интеракцијата човек-компјутер', null, null, '2022-23-S', 'F23L3S010', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Континуирана интеграција и испорака', null, null, '2022-23-S', 'F23L3S118', null, null);
INSERT INTO public.course (id, total_students, total_teaching_staff, semester_code, subject_id, subject2_id,
                           subject3_id)
VALUES ('Оперативни системи', null, null, '2022-23-S', 'F23L2S017', null, null);

INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (1, 'Веб програмирање', 'riste.stojanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (2, 'Веб програмирање', 'sasho.gramatikov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (3, 'Веб програмирање', 'dimitar.trajanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (4, 'Вовед во науката за податоци', 'dimitar.trajanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (5, 'Вовед во науката за податоци', 'igor.mishkovski');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (6, 'Вовед во науката за податоци', 'andrea.kulakov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (7, 'Електронска и мобилна трговија', 'riste.stojanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (8, 'Електронска и мобилна трговија', 'dimitar.trajanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (9, 'Електронска и мобилна трговија', 'sasho.gramatikov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (10, 'Континуирана интеграција и испорака', 'milos.jovanovik');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (11, 'Континуирана интеграција и испорака', 'pance.ribarski');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (12, 'Оперативни системи', 'riste.stojanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (13, 'Оперативни системи', 'dimitar.trajanov');
INSERT INTO public.course_group (id, course_id, professor_id)
VALUES (14, 'Оперативни системи', 'sasho.gramatikov');




INSERT INTO public.semester_exam_session (name, session, semester_code, start, "end", enrollment_start_date, enrollment_end_date, year) VALUES ('2022-23-W-FIRST_MIDTERM', 'FIRST_MIDTERM', null, '2021-08-05', null, null, null, '2022-23');
INSERT INTO public.semester_exam_session (name, session, semester_code, start, "end", enrollment_start_date, enrollment_end_date, year) VALUES ('2022-23-W-JUNE', 'JUNE', null, '2023-08-17', null, null, null, '2022-23');
INSERT INTO public.semester_exam_session (name, session, semester_code, start, "end", enrollment_start_date, enrollment_end_date, year) VALUES ('2022-23-W-JANUARY', 'JANUARY', null, '2023-08-06', null, null, null, '2022-23');
INSERT INTO public.semester_exam_session (name, session, semester_code, start, "end", enrollment_start_date, enrollment_end_date, year) VALUES ('2022-23-W-SEPTEMBER', 'SEPTEMBER', null, '2022-08-10', null, null, null, '2022-23');
INSERT INTO public.semester_exam_session (name, session, semester_code, start, "end", enrollment_start_date, enrollment_end_date, year) VALUES ('2022-23-W-SECOND_MIDTERM', 'SECOND_MIDTERM', null, '2019-08-02', null, null, null, '2022-23');

INSERT INTO public.semester (code) VALUES ('2022-23-S');
INSERT INTO public.semester (code) VALUES ('2022-23-W');



INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (1, null, '2022-23-W-JUNE', null, 1);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (2, null, '2022-23-W-JANUARY', null, 1);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (3, null, '2022-23-W-SEPTEMBER', null, 1);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (4, null, '2022-23-W-FIRST_MIDTERM', null, 1);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (5, null, '2022-23-W-SECOND_MIDTERM', null, 1);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (6, null, '2022-23-W-JUNE', null, 1);

INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (7, null, '2022-23-W-JUNE', null, 4);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (8, null, '2022-23-W-JANUARY', null, 7);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (9, null, '2022-23-W-SEPTEMBER', null, 10);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (10, null, '2022-23-W-JUNE', null, 12);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (11, null, '2022-23-W-FIRST_MIDTERM', null, 2);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (12, null, '2022-23-W-SECOND_MIDTERM', null, 5);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (13, null, '2022-23-W-FIRST_MIDTERM', null, 7);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (14, null, '2022-23-W-JUNE', null, 4);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (15, null, '2022-23-W-SEPTEMBER', null, 3);
INSERT INTO public.results (id, pdf_bytes, session_name, uploaded_at, course_group_id)
VALUES (16, null, '2022-23-W-JANUARY', null, 5);
